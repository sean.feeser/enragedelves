﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SniperElfController : MonoBehaviour
{

    [Header("Attack Settings")]
    public float attackFrequency = 1f;
    public float aimSpeed = 1f;
    [SerializeField]EnemyTarget target;

    [Header("Patrol References")]
    public SniperPatrolTarget patrolTarget;


    [Header("Bullet References")]
    public Transform bulletSpawn;
    public GameObject bulletPrefab;

    bool isAggroed = false;
    Coroutine attackCoroutine, deaggroCoroutine;
    Animator anim;
    LineRenderer lr;
    Vector3 dir;
    Vector3 point;
    Camera camera;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponentInChildren<Animator>();
        lr = GetComponentInChildren<LineRenderer>();
        camera = GetComponentInChildren<Camera>();
        target = EnemyTarget._instance;
    }

    // Update is called once per frame
    void Update()
    {
        if (isAggroed)
        {
            Vector3 delta = target.position - bulletSpawn.position;
            dir = Vector3.Slerp(dir, delta, aimSpeed * Time.deltaTime);
        }
        else
        {
            Vector3 delta = patrolTarget.position - bulletSpawn.position;
            dir = Vector3.Slerp(dir, delta, aimSpeed * Time.deltaTime);
        }
    }

    private void LateUpdate()
    {
        lr.SetPosition(0, bulletSpawn.position);
        lr.SetPosition(1, bulletSpawn.position + (dir * 1000f));
        FaceDirection(dir);
        CheckTargetInSight();
    }

    public void AggroTarget()
    {
        isAggroed = true;
        //this.target = target;
        attackCoroutine = StartCoroutine(ShootAtTarget());
        if (deaggroCoroutine != null)
            StopCoroutine(deaggroCoroutine);
        Debug.Log("Elf is pissed");
    }

    public void DeaggroTarget()
    {
        isAggroed = false;
        deaggroCoroutine = StartCoroutine(DelayDeaggro(3f));
    }

    IEnumerator ShootAtTarget()
    {
        while (isAggroed)
        {
            yield return new WaitForSeconds(attackFrequency);
            lr.enabled = false;
            yield return new WaitForSeconds(0.1f);
            FireBullet();
            yield return new WaitForSeconds(0.1f);
            lr.enabled = true;
        }
    }

    IEnumerator DelayDeaggro(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        if (attackCoroutine != null)
            StopCoroutine(attackCoroutine);
        //point = startLook.position;
        Debug.Log("Elf is cool again");
    }

    void FireBullet()
    {
        anim.SetTrigger("Shoot");
        Instantiate(bulletPrefab, bulletSpawn.position, Quaternion.LookRotation(dir));
    }

    void FaceDirection(Vector3 dir)
    {
        Vector3 look = new Vector3(dir.x, 0, dir.z);
        transform.rotation = Quaternion.LookRotation(look, Vector3.up);
        camera.transform.rotation = Quaternion.LookRotation(dir);
    }

    void CheckTargetInSight()
    {
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(camera);
        if (GeometryUtility.TestPlanesAABB(planes, target.collider.bounds) && isAggroed == false)
        {
            RaycastHit hit;
            Vector3 delta = target.position - camera.transform.position;
            Debug.DrawLine(camera.transform.position, camera.transform.position + (delta.normalized * camera.farClipPlane), Color.green);
            if (Physics.Raycast(camera.transform.position, delta.normalized, out hit))
            {

                if (hit.transform == target.transform)
                    AggroTarget();
            }
        }
        if (GeometryUtility.TestPlanesAABB(planes, target.collider.bounds) == false && isAggroed)
            DeaggroTarget();
    }
}
