﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SniperPatrolTarget : MonoBehaviour
{

    public float width = 1f;
    public float speed = 1f;

    public Vector3 position
    {
        get
        {
            return transform.position;
        }
    }

    Vector3 startPos;
    Vector3 endPos;

    Vector3 dir;
    bool forward = true;

    // Start is called before the first frame update
    void Start()
    {
        transform.parent = null;//Become Batman
        startPos = transform.position;
        endPos = transform.position + (transform.right * width);
        dir = endPos - startPos;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += dir * speed * Time.deltaTime;
        if(forward && Vector3.Distance(transform.position, endPos) < 0.1f)
        {
            dir *= -1;
            forward = false;
        }
        if(forward == false && Vector3.Distance(transform.position, startPos) < 0.1f)
        {
            dir *= -1;
            forward = true;
        }
    }

    private void OnDrawGizmos()
    {
        if (Application.isPlaying == false)
        {
            startPos = transform.position;
            endPos = transform.position + (transform.right * width);
        }

        Gizmos.color = Color.red;
        Gizmos.DrawLine(startPos, endPos);
    }
}
