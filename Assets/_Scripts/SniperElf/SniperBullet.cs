﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SniperBullet : MonoBehaviour
{

    public float speed;
    public float lifetime;

    private void Start()
    {
        Invoke("Destroy", lifetime);
    }


    // Update is called once per frame
    void Update()
    {
        transform.position += transform.forward * speed * Time.deltaTime;
    }

    void Destroy()
    {
        Destroy(gameObject);
    }
}
