﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class EnemyTarget : MonoBehaviour
{

    public static EnemyTarget _instance;

    public Collider collider
    {
        get; protected set;
    }

    public Vector3 position
    {
        get
        {
            return transform.position;
        }
    }

    // Start is called before the first frame update
    void Awake()
    {
        collider = GetComponent<Collider>();
        _instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
