﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolElfAnimationListener : MonoBehaviour
{

    PatrolElfController elf;

    private void Start()
    {
        elf = GetComponentInParent<PatrolElfController>();
    }

    public void AggroStart()
    {
        Debug.Log("Start Aggro");
        elf.StopMoving();
    }

    public void AggroStop()
    {
        Debug.Log("Stop Aggro");
        elf.StartMoving();
    }

    public void StartDeaggro()
    {
        Debug.Log("Start Deaggro");
        elf.StopMoving();
    }

    public void StopDeaggro()
    {
        Debug.Log("Stop Deaggro");
        elf.StartMoving();
    }
}
