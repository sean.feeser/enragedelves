﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class PatrolElfController : MonoBehaviour
{

    [Header("Patrol Settings")]
    public float walkSpeed = 1f;
    public float chaseSpeed = 2f;

    [Header("Attack Settings")]
    public float attackRange = 1f;
    public float attackDelay = 0.5f;
    [SerializeField] EnemyTarget target;

    [Header("Patrol References")]
    public Transform[] waypoints;

    Transform currentWaypoint;
    int currentWaypointIndex;
    NavMeshAgent nav;
    Animator anim;
    Camera camera;

    Coroutine attackCoroutine, deaggroCoroutine;



    bool isAggroed = false;

    // Start is called before the first frame update
    void Start()
    {
        nav = GetComponent<NavMeshAgent>();
        anim = GetComponentInChildren<Animator>();
        camera = GetComponentInChildren<Camera>();
        currentWaypoint = waypoints[0];
        target = EnemyTarget._instance;
    }

    // Update is called once per frame
    void Update()
    {
        float targetDistance = Vector3.Distance(transform.position, target.position);
        if (isAggroed)
        {
            nav.SetDestination(target.position);
            Debug.Log("Nav " + nav.pathStatus);
            Debug.DrawLine(transform.position, target.position, Color.blue);
        }
        else
        {
            if (nav.remainingDistance <= 0.1f)
            {
                SetNextDestination();
            }
        }
    }

    private void LateUpdate()
    {
        CheckTargetInSight();
    }

    public void SetNextDestination()
    {
        currentWaypoint = GetNextWaypoint();
        nav.SetDestination(currentWaypoint.position);
    }

    public void SetAggro(bool value)
    {
        isAggroed = value;
        //if (isAggroed)
        //{
        //    nav.speed = chaseSpeed;
        //}
        //else
        //{
        //    nav.speed = walkSpeed;
        //}
    }

    public void StopMoving()
    {
        nav.isStopped = true;
    }

    public void StartMoving()
    {
        nav.isStopped = false;
    }

    public void AggroTarget()
    {
        SetAggro(true);
        anim.SetTrigger("Aggro");
        //nav.SetDestination(GetNavPos(target.position));
        StopMoving();
        //this.target = target;
        //attackCoroutine = StartCoroutine(ShootAtTarget());
        if (deaggroCoroutine != null)
            StopCoroutine(deaggroCoroutine);
        Debug.Log("Elf is pissed");
    }

    public void DeaggroTarget()
    {
        SetAggro(false);
        deaggroCoroutine = StartCoroutine(DelayDeaggro(3f));
    }


    IEnumerator DelayDeaggro(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        anim.SetTrigger("Deaggro");
        SetNextDestination();
        if (attackCoroutine != null)
            StopCoroutine(attackCoroutine);
        //point = startLook.position;
        Debug.Log("Elf is cool again");
    }

    bool isIncrementing;
    Transform GetNextWaypoint()
    {
        if (isIncrementing)
        {
            currentWaypointIndex++;
            if (currentWaypointIndex >= waypoints.Length)
            {
                currentWaypointIndex--;
                isIncrementing = false;
            }
        }
        else
        {
            currentWaypointIndex--;
            if (currentWaypointIndex < 0)
            {
                currentWaypointIndex++;
                isIncrementing = true;
            }
        }
        return waypoints[currentWaypointIndex];
    }

    Vector3 GetNavPos(Vector3 pos)
    {
        NavMeshHit myNavHit;
        if (NavMesh.SamplePosition(transform.position, out myNavHit, 100, -1))
        {
            return myNavHit.position;
        }
        Debug.LogError("PatrolElfController: Can't reach position " + pos);
        return pos;
    }

    void CheckTargetInSight()
    {
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(camera);
        if (GeometryUtility.TestPlanesAABB(planes, target.collider.bounds) && isAggroed == false)
        {
            RaycastHit hit;
            Vector3 delta = target.position - camera.transform.position;
            Debug.DrawLine(camera.transform.position, camera.transform.position + (delta.normalized * camera.farClipPlane), Color.green);
            if (Physics.Raycast(camera.transform.position, delta.normalized, out hit))
            {

                if (hit.transform == target.transform)
                    AggroTarget();
            }
        }
        if (GeometryUtility.TestPlanesAABB(planes, target.collider.bounds) == false && isAggroed)
            DeaggroTarget();
    }
}
